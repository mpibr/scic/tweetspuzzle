const fs = require('fs');

// Read tweets from JSON file
const tweets = JSON.parse(fs.readFileSync('db/tweets.json', 'utf-8'));

// Read icons
const iconHearth = fs.readFileSync('public/hearth.svg', 'utf-8');
const iconRetweet = fs.readFileSync('public/retweet.svg', 'utf-8');

// Function to generate random hex color
const getRandomColor = () => Math.floor(Math.random() * 16777215).toString(16);

// Function to generate random number between min and max
const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

// Function to get avatar URL
const getAvatarURL = (name) => `https://ui-avatars.com/api/?name=${encodeURIComponent(name)}&color=${getRandomColor()}&format=svg&background=ffffff`;

// Function to shuffle characters in a string
const shuffleString = (str) => {
    const arr = str.split('');
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr.join('');
};

// Function to convert name to username format
const convertToUsername = (name) => {
    return name.replace(/\s+/g, '').replace(/^[a-z]/, (char) => char.toLowerCase());
}

// Shuffle letters in the names of the tweets
const shuffleTweetNames = (tweets) => {
    return tweets.map((tweet) => {
        const originalName = tweet.name;
        const shuffledName = shuffleString(tweet.name);
        const username = convertToUsername(shuffledName);
        return {
            ...tweet,
            username: username
        };
    });
};


// HTML template for a tweet
const generateTweetHTML = (tweet) => `
    <div class="tweet">
        <div class="tweet-header">
            <img src="${getAvatarURL(tweet.name)}" alt="Profile Picture">
            <div>
                <span class="name">${tweet.name}</span>
                <span class="username">@${tweet.username}</span>
            </div>
        </div>
        <div class="tweet-text">
            ${tweet.text}
        </div>
        <div class="tweet-hashtags">
            ${tweet.hashtags.map(tag => `<a href="#">${tag}</a>`).join(' ')}
        </div>
        <div class="tweet-footer">
            <span class="date">14 Jun 2024</span>
            <span class="likes"> <i>${iconHearth}</i> ${getRandomNumber(10, 100)}</span>
            <span class="reshares"> <i>${iconRetweet}</i> ${getRandomNumber(10, 100)}</span>
        </div>
    </div>
`;

const generateHTMLContent = (tweets) =>`
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tweets</title>
    <link rel="stylesheet" href="css/tweets.css">
</head>
<body>
    <div class="container">
    ${tweets.map((shuffledTweets) => `
        <div class="column">
            ${generateTweetHTML(shuffledTweets)}
        </div>
    `).join('')}
    </div>
</body>
</html>
`;

// Write HTML content to a file
const shuffledTweets = shuffleTweetNames(tweets);
const htmlContent = generateHTMLContent(shuffledTweets);
fs.writeFileSync('export/tweets.html', htmlContent);

console.log('HTML file has been generated successfully.');
