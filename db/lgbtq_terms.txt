1. **Ace**: An abbreviation for asexual, a person who experiences little or no sexual attraction.
2. **Ag / Aggressive**: A term used primarily in the Black and Latinx LGBTQ+ communities to describe a masculine-presenting lesbian.
3. **Agender**: A person who identifies as having no gender or being gender-neutral.
4. **Alloromantic**: A person who experiences romantic attraction.
5. **Allosexual**: A person who experiences sexual attraction.
6. **Ally**: A person who supports and stands up for the rights of LGBTQ+ people.
7. **Androgyne**: A person whose gender identity is a mix of both male and female, or who is neither male nor female.
8. **Aro / Aromantic**: A person who experiences little or no romantic attraction to others.
9. **Asexual**: A person who experiences little or no sexual attraction to others.
10. **Assigned at Birth**: The gender that a person is designated as being at birth, typically based on physical anatomy.
11. **Bear**: A gay man who is typically large, hairy, and has a rugged or masculine appearance.
12. **Bicurious**: A person who is exploring or curious about bisexuality but does not necessarily identify as bisexual.
13. **Bigender**: A person who identifies as two genders, either simultaneously or at different times.
14. **Binding**: The practice of flattening one's chest using constrictive materials to achieve a more masculine appearance.
15. **Boi**: A term used within the LGBTQ+ community, often referring to a young, gender non-conforming, or masculine-presenting person.
16. **Bottom**: A person who prefers the receptive role in sexual activities.
17. **Cisnormativity**: The assumption that being cisgender (identifying with the gender assigned at birth) is the norm.
18. **Demiromantic**: A person who experiences romantic attraction only after forming a strong emotional bond with someone.
19. **Demisexual**: A person who experiences sexual attraction only after forming a strong emotional bond with someone.
20. **Femme**: A person who identifies with or presents in a traditionally feminine manner.
21. **FTM/MTF**: Female-to-male (FTM) or male-to-female (MTF), terms used to describe the direction of a transgender person's transition.
22. **Gender Binary**: The classification of gender into two distinct and opposite forms of masculine and feminine.
23. **Gender Dysphoria**: Distress experienced by a person due to a mismatch between their gender identity and their sex assigned at birth.
24. **Gender Expression**: The external display of one's gender, through behavior, clothing, haircut, voice, and other forms of presentation.
25. **Gender Identity**: A person's deeply-felt internal experience of gender, which may be different from their sex assigned at birth.
26. **Gender Non-Conforming**: A person whose gender expression does not align with societal expectations based on their sex assigned at birth.
27. **Gender Normative**: Adhering to or conforming with the cultural expectations of one's assigned gender at birth.
28. **Gender Variance**: Behavior or gender expression that does not conform to dominant gender norms.
29. **Genderqueer**: A person who does not subscribe to conventional gender distinctions but identifies with neither, both, or a combination of male and female genders.
30. **Grey Ace**: A person who experiences sexual attraction infrequently or only under specific circumstances.
31. **Greyromantic**: A person who experiences romantic attraction infrequently or only under specific circumstances.
32. **Heteronormativity**: The belief that heterosexuality is the default, preferred, or normal mode of sexual orientation.
33. **Intersex**: A person born with physical sex characteristics that do not fit typical binary notions of male or female bodies.
34. **Masculine of Center**: A term describing individuals who identify or present toward the masculine side of the gender spectrum.
35. **MSM**: Men who have sex with men, regardless of their sexual orientation.
36. **Nonmonosexual**: An umbrella term for people who are attracted to more than one gender.
37. **Neutrois**: A non-binary gender identity that is neutral or null.
38. **Panromantic**: A person who is romantically attracted to others regardless of their gender.
39. **Pansexual**: A person who is sexually attracted to others regardless of their gender.
40. **Polyamory**: The practice of engaging in multiple consensual romantic or sexual relationships simultaneously.
41. **Polysexual**: A person who is attracted to multiple, but not all, genders.
42. **Queerplatonic**: A relationship that is more intense or emotionally significant than a typical friendship but does not fit conventional romantic norms.
43. **Sex Identity**: The biological characteristics (genitals, chromosomes, hormones) that society typically uses to assign gender at birth.
44. **Squish**: A platonic crush, or an intense desire to be close friends with someone.
45. **Stealth**: A term used to describe a transgender person who does not disclose their transgender status in their everyday life.
46. **Stem**: A person who exhibits both feminine and masculine characteristics, often within the lesbian community.
47. **Third Gender**: A gender category that exists in many cultures, recognizing genders outside the binary of male and female.
48. **Top**: A person who prefers the insertive role in sexual activities.
49. **Trans***: An inclusive term for people whose gender identity differs from their sex assigned at birth.
50. **Trans feminine**: A transgender person who identifies with femininity or a female gender.
51. **Trans masculine**: A transgender person who identifies with masculinity or a male gender.
52. **Two-Spirit**: A term used by some Indigenous North American cultures to describe a person who embodies both masculine and feminine qualities.
53. **WSW**: Women who have sex with women, regardless of their sexual orientation.
